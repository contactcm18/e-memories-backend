import mongoose, { Schema, Document } from "mongoose";

export const PASSWORD_MIN_LENGTH = 8;

export interface IUser extends Document {
  email: string;
  password: string;
  refreshToken: string;
  dateCreated: Number;
}

const userSchema = new Schema({
  email: {
    type: String,
    required: [true, "Email is required."],
    validate: {
      validator: (value: string) => /^\S+@\S+\.\S+$/.test(value),
      message: (props: any) => `'${props.value}' is not a valid email.`,
    },
  },
  password: {
    type: String,
    required: true,
    minlength: PASSWORD_MIN_LENGTH,
  },
  refreshToken: {
    type: String,
    required: true,
  },
  dateCreated: {
    type: Number,
    required: true,
    default: () => Date.now(),
  },
});

export const User = mongoose.model<IUser>("E-Memories User", userSchema);
