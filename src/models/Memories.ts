import mongoose, { Schema, Document } from "mongoose";

import { ReceiveMemoryEvery } from "../lib/user";

interface IMemories extends Document {
  owner: string;
  memories: Array<{ id: string; text: string; dateCreated: number }>;
  receiveEvery: ReceiveMemoryEvery;
  lastMemorySent: number;
}

const memoriesSchema: Schema = new Schema({
  owner: {
    type: String,
    required: true,
  },
  memories: {
    type: Array,
    required: true,
    default: [],
  },
  receiveEvery: {
    type: ReceiveMemoryEvery,
    required: true,
    default: ReceiveMemoryEvery.THREE_DAYS,
    validate: {
      validator: (value: string) =>
        Object.values(ReceiveMemoryEvery as any).includes(value),
    },
  },
  lastMemorySent: {
    type: Number,
    required: true,
    default: () => Date.now(),
  },
});

export default mongoose.model<IMemories>("Memories", memoriesSchema);
