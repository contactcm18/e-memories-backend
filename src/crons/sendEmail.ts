import Memories from "../models/Memories";

import { getMilliTime } from "../lib/user";
import { sendEmail } from "../lib/auth";

export default async () => {
  const userMemories = await Memories.find();
  if (!userMemories.length) return;

  const timeNow = Date.now();

  for (let i = 0; i < userMemories.length; i++) {
    if (
      timeNow - (userMemories[i].lastMemorySent - 10000) >=
        getMilliTime(userMemories[i].receiveEvery) &&
      userMemories[i].memories.length
    ) {
      const randomMemory =
        userMemories[i].memories[
          Math.floor(Math.random() * (userMemories[i].memories.length - 1))
        ];

      if (randomMemory) {
        sendEmail(
          userMemories[i].owner,
          "A Special Memory Delivery",
          `
            <h2><strong>"${randomMemory.text}"</strong></h2>
            <p>${new Date(randomMemory.dateCreated).toDateString()}</p>
            <a href="https://e-memories.designtoweb.dev/memories">Create a new memory</a>
          `
        ).then(({ statusCode }) => {
          if (statusCode === 200) {
            userMemories[i].lastMemorySent = timeNow;
            userMemories[i].save();
          }
        });
      }
    }
  }
};
