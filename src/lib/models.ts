/**
 * Returns array of error messages.
 * @param {*} errors The errors object, most likely error.errors.
 */
export const getModelErrors = (errors: any) => {
  let errorMessages = [];
  for (let key in errors) {
    errorMessages.push(errors[key].message);
  }
  return errorMessages;
};
