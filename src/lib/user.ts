export enum ReceiveMemoryEvery {
  DAY = "Day",
  THREE_DAYS = "Three Days",
  WEEK = "Week",
  MONTH = "Month",
}

export const getMilliTime = (Time: ReceiveMemoryEvery) => {
  const ONE_DAY = 24 * 60 * 60 * 1000;
  switch (Time) {
    case ReceiveMemoryEvery.DAY:
      return ONE_DAY;
    case ReceiveMemoryEvery.THREE_DAYS:
      return ONE_DAY * 3;
    case ReceiveMemoryEvery.WEEK:
      return ONE_DAY * 7;
    case ReceiveMemoryEvery.MONTH:
      return ONE_DAY * 30;
    default:
      return 0;
  }
};

const quotes = [
  {
    text:
      "Sometimes you will never know the true value of a moment until it becomes a memory.",
    author: "Theodor Seuss Geisel",
  },
  {
    text:
      "So long as the memory of certain beloved friends lives in my heart, I shall say that life is good.",
    author: "Helen Keller",
  },
  {
    text: "Each happiness of yesterday is a memory for tomorrow.",
    author: "Unknown",
  },
  {
    text: "Don't cry because it's over; smile because it happened.",
    author: "Dr. Seuss",
  },
  {
    text:
      "Our memory is a more perfect world than the universe: it gives back life to those who no longer exist.",
    author: "Guy de Maupassant",
  },
  {
    text:
      "Memories, even your most precious ones, fade surprisingly quickly. But I don’t go along with that. The memories I value most, I don’t ever see them fading.",
    author: "Kazuo Ishiguro, Never Let Me Go",
  },
  {
    text: "Scars have the strange power to remind us that our past is real.",
    author: "Cormac McCarthy, All the Pretty Horses",
  },
  {
    text: "One of the keys to happiness is a bad memory.",
    author: "Rita Mae Brown",
  },
  {
    text: "One lives in the hope of becoming a memory.",
    author: "Antonio Porchia",
  },
];

export const getRandomQuote = () =>
  quotes[Math.floor(Math.random() * quotes.length)];
