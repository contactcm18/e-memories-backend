import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import nodemailer from "nodemailer";

const ACCESS_TOKEN_EXPIRE: string = "5m";
const REFRESH_TOKEN_EXPIRE: string = "7d";

export const hashPassword = async (password: string) => {
  const salt = await bcrypt.genSalt();
  return new Promise<string>(function (resolve, reject) {
    bcrypt
      .hash(password, salt)
      .then((data) => resolve(data))
      .catch(() => reject());
  });
};

export const isPasswordValid = (password: string) =>
  password && password.length >= minPasswordLength;

export const minPasswordLength = 8;

export interface ITokenRequire {
  email: string;
  _id: string;
}
/**
 * Creates and returns a signed JWT.
 * @param {String} email
 */
export const createAccessToken = ({ email, _id }: ITokenRequire) =>
  jwt.sign({ email, _id }, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: ACCESS_TOKEN_EXPIRE,
  });

/**
 * Creates and returns a signed JWT.
 * @param {String} email
 */
export const createRefreshToken = ({ email, _id }: ITokenRequire) =>
  jwt.sign({ email, _id }, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: REFRESH_TOKEN_EXPIRE,
  });

/**
 * Attaches accessToken cookie and refreshToken cookie to res.
 * @param {Response<any, number>} res
 * @param {String} accessToken
 * @param {String} refreshToken
 */
export const attachCookies = (
  res: any,
  accessToken: string,
  refreshToken: string
) => {
  const cookieOptions = {
    domain: getDomain(),
    httpOnly: true,
    secure: enableSecureCookie(),
    sameSite: "Lax",
    maxAge: 24 * 60 * 60 * 1000,
  };
  res.cookie("accessToken", accessToken, cookieOptions);
  res.cookie("refreshToken", refreshToken, cookieOptions);
};

const getDomain = () =>
  process.env.HOST.includes("localhost")
    ? "localhost"
    : `.${process.env.HOST.match(/\w*\.\w*$/g)[0]}`;

const enableSecureCookie = () => process.env.HOST.includes("https");

export const isValidEmail = (email: string) => /^\S+@\S+\.\S+$/g.test(email);

export const sendEmail = async (to: string, subject: string, body: string) => {
  const transporter = nodemailer.createTransport({
    host: "smtp-relay.sendinblue.com",
    port: 587,
    secure: false,
    auth: {
      user: process.env.MAIL_OWNER,
      pass: process.env.MAIL_PASS,
    },
  });

  const verificationResult = await transporter.verify();
  if (!verificationResult)
    return {
      statusCode: 400,
      text: "Verification of transport failed.",
    };

  const mailOptions = {
    from: process.env.MAIL_FROM,
    to,
    subject,
    text: body,
  };

  try {
    await transporter.sendMail(mailOptions);
  } catch (error) {
    return {
      statusCode: 400,
      text: "Send mail failed: " + error,
    };
  }

  return {
    statusCode: 200,
    text: "Mail successfully sent.",
  };
};
