import express from "express";
import uniqid from "uniqid";

import Memories from "../models/Memories";
import { authenticated } from "../middleware/authenticated";
import { getRandomQuote, ReceiveMemoryEvery } from "../lib/user";

const router = express.Router();

router.get("/getQuote", (req, res) => {
  return res.json(getRandomQuote());
});

router.get("/getMemories", authenticated, async (req, res) => {
  const { email } = (req as any).user;

  let memoriesObj = await Memories.findOne({ owner: email });

  if (!memoriesObj) {
    memoriesObj = new Memories({ owner: email });
    await memoriesObj.save();
  }

  return res.json({
    memories: memoriesObj.memories,
    receiveTime: memoriesObj.receiveEvery,
  });
});

router.post("/addMemory", authenticated, async (req, res) => {
  const { memory } = req.body;
  if (!memory) return res.sendStatus(400);

  const { email } = (req as any).user;

  const memoriesObj = await Memories.findOne({ owner: email });
  if (!memoriesObj) return res.status(401);

  const newMemory = {
    id: uniqid(),
    text: memory,
    dateCreated: Date.now(),
  };

  memoriesObj.memories = [newMemory, ...memoriesObj.memories];
  memoriesObj
    .save()
    .then(() => {
      return res.json(newMemory);
    })
    .catch(() => {
      return res.sendStatus(500);
    });
});

router.delete("/deleteMemory", authenticated, async (req, res) => {
  const { memoryId } = req.body;
  if (!memoryId) return res.sendStatus(400);

  const { email } = (req as any).user;

  const memoriesObj = await Memories.findOne({ owner: email });
  if (!memoriesObj) return res.status(401);

  memoriesObj.memories = memoriesObj.memories.filter(
    (memory) => memory.id !== memoryId
  );
  memoriesObj
    .save()
    .then(() => {
      return res.sendStatus(200);
    })
    .catch(() => {
      return res.sendStatus(500);
    });
});

router.get("/memoryReceiveTimes", (req, res) => {
  return res.json(ReceiveMemoryEvery);
});

router.post("/setReceiveTime", authenticated, async (req, res) => {
  const { receiveTime } = req.body;
  if (!receiveTime) return res.sendStatus(400);

  const { email } = (req as any).user;
  const memories = await Memories.findOne({ owner: email });
  if (!memories) return res.sendStatus(404);

  memories.receiveEvery = receiveTime;
  memories
    .save()
    .then(() => {
      return res.sendStatus(200);
    })
    .catch(() => {
      return res.sendStatus(403);
    });
});

export default router;
