import express from "express";
const router = express.Router();
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import { User, PASSWORD_MIN_LENGTH } from "../models/User";
import Memories from "../models/Memories";
import {
  isValidEmail,
  createRefreshToken,
  createAccessToken,
  attachCookies,
  isPasswordValid,
  hashPassword,
  sendEmail,
} from "../lib/auth";
import { getModelErrors } from "../lib/models";
import { authenticated } from "../middleware/authenticated";

router.post("/isAuthenticated", authenticated, (req, res) => {
  return res.json((req as any).user);
});

router.post("/register", async (req, res) => {
  const { email, password1, password2 } = req.body;

  const miscErrors: string[] = [];
  // Custom validation not used in Model
  if (!password1 || !password2 || password1 !== password2)
    miscErrors.push("Passwords do not match.");
  if (!isPasswordValid(password1))
    miscErrors.push(
      `Password must be at least ${PASSWORD_MIN_LENGTH} characters in length.`
    );

  let hashedPassword = null;
  try {
    hashedPassword = await hashPassword(password1);
  } catch (error) {
    return res.sendStatus(400);
  }

  // Does user with email already exist?
  const existingUser = await User.findOne({ email });
  if (existingUser) miscErrors.push("Account with email already exists.");

  const user = new User({
    email,
    password: hashedPassword,
  });
  user.refreshToken = createRefreshToken({ email, _id: user._id });

  // Validate fields
  try {
    await user.validate();
  } catch (error) {
    return res
      .status(400)
      .json([...getModelErrors(error.errors), ...miscErrors]);
  }

  // If we caught any of our own errors, send them
  if (miscErrors.length) return res.status(400).json(miscErrors);

  user
    .save()
    .then(() => {
      attachCookies(
        res,
        createAccessToken({ email, _id: user._id }),
        user.refreshToken
      );
      return res.json({ email });
    })
    .catch((error) => {
      console.log(error);
      return res
        .status(400)
        .json([...getModelErrors(error.errors), ...miscErrors]);
    });
});

router.post("/login", async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password)
    return res
      .status(400)
      .json([
        ...(!email ? ["Please enter an email."] : []),
        ...(!password ? ["Please enter a password."] : []),
      ]);

  if (!isValidEmail(email))
    return res.status(400).json(["Please enter a valid email."]);

  // Get user from DB
  const user = await User.findOne({ email });
  if (!user) return res.status(400).json(["User does not exist."]);

  // Compare passwords
  const match = await bcrypt.compare(password, user.password);
  if (!match) return res.status(400).json(["Incorrect password."]);

  const newRefreshToken = createRefreshToken({ email, _id: user._id });
  await user.updateOne({ $set: { refreshToken: newRefreshToken } });

  attachCookies(
    res,
    createAccessToken({ email, _id: user._id }),
    newRefreshToken
  );

  return res.json({ email });
});

router.post("/logout", (req, res) => {
  attachCookies(res, "", "");
  return res.sendStatus(200);
});

router.put("/resetPassword", async (req, res) => {
  const { email } = req.body;
  if (!email) return res.sendStatus(400);

  const user = await User.findOne({ email });
  if (!user) return res.sendStatus(404);

  const jwtPasswordReset = jwt.sign(
    { currentPassword: user.password, userDateCreated: user.dateCreated },
    process.env.RESET_PASSWORD_SECRET
  );

  const response = await sendEmail(
    email,
    "E-memories Password Reset",
    `
      <p>If you did not request a password reset, you can safely ignore this email.</p>
      <a href="${process.env.CLIENT_HOST}/resetPassword/${jwtPasswordReset}" target="_blank">Otherwise, reset password</a>
    `
  );

  return res.sendStatus(response.statusCode);
});

router.put("/resetPassword/:jwtPasswordReset", async (req, res) => {
  const { password1, password2 } = req.body;
  if (!isPasswordValid(password1))
    return res
      .status(400)
      .json([
        `Password must be at least ${PASSWORD_MIN_LENGTH} characters in length.`,
      ]);

  if (password1 !== password2)
    return res.status(400).json(["Passwords do not match."]);

  const { jwtPasswordReset } = req.params;

  try {
    jwt.verify(jwtPasswordReset, process.env.RESET_PASSWORD_SECRET);
  } catch (error) {
    return res
      .status(403)
      .json(["An error occurred. This operation is forbidden."]);
  }

  const { currentPassword, userDateCreated } = jwt.decode(
    jwtPasswordReset
  ) as any;
  const user = await User.findOne({
    password: currentPassword,
    dateCreated: userDateCreated,
  });
  if (!user) return res.sendStatus(404);

  try {
    const hashedPassword = await hashPassword(password1);
    user.password = hashedPassword;

    user
      .save()
      .then(() => {
        return res.sendStatus(200);
      })
      .catch((error) => {
        return res.status(400).json(getModelErrors(error.errors));
      });
  } catch (error) {
    return res.sendStatus(400);
  }
});

router.delete("/deleteAccount", authenticated, async (req, res) => {
  const { confirmationEmail } = req.body;
  if (!confirmationEmail) return res.sendStatus(400);

  const { email } = (req as any).user;
  if (email !== confirmationEmail) return res.sendStatus(403);

  try {
    Memories.deleteOne({ owner: email }, {}, (err) => {
      if (err) throw Error;
      User.deleteOne({ email }, {}, (err) => {
        if (err) throw Error;
        attachCookies(res, "", "");
        return res.sendStatus(200);
      });
    });
  } catch (error) {
    return res.sendStatus(500);
  }
});

export default router;
