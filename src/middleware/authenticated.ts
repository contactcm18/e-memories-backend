const jwt = require("jsonwebtoken");

const { User } = require("../models/User");
const {
  createAccessToken,
  createRefreshToken,
  attachCookies,
} = require("../lib/auth");

/**
 * Only calls next() if user is authenticated.
 * If access token is invalid but refresh token is valid,
 * the user is authenticated and the tokens are updated.
 * User is attached to req object.
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const authenticated = async (req: any, res: any, next: any) => {
  const { accessToken, refreshToken } = req.cookies;
  if (!accessToken || !refreshToken) return res.sendStatus(401);

  try {
    jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET);
  } catch (error) {
    // Access token is invalid...
    try {
      // Does user with this refreshToken exist?
      const user = await User.findOne({ refreshToken });
      if (!user)
        throw Error(
          `User with refresh token (${refreshToken}) does not exist.`
        );

      jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET);

      // Refresh token is valid! Create new access token & refresh token
      const newRefreshToken = createRefreshToken(user);
      await user.updateOne({
        $set: { refreshToken: newRefreshToken },
      });

      attachCookies(res, createAccessToken(user), newRefreshToken);
    } catch (error) {
      console.log(error);
      attachCookies(res, "", "");
      return res.sendStatus(401);
    }
  }

  req.user = jwt.decode(refreshToken);

  next();
};
