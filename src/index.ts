require("dotenv").config();
import express from "express";
const app = express();
import mongoose from "mongoose";
import cookieParser from "cookie-parser";
import cors from "cors";
import cron from "node-cron";

import sendEmail from "./crons/sendEmail";

import authRoute from "./routes/auth";
import userRoute from "./routes/user";

const PORT = process.env.PORT || "4000";
app.listen(PORT, () => console.log(`Server up and running on port ${PORT}`));

mongoose.connect(
  process.env.DB_CONNECT_URI,
  { useNewUrlParser: true, useUnifiedTopology: true },
  function (err) {
    if (err) {
      console.error("Error in connecting to DB.", err);
      return;
    }
    console.log("Connected to DB.");
  }
);

// Middleware
app.use(
  cors({
    origin: process.env.CORS_ORIGIN,
    optionsSuccessStatus: 200,
    credentials: true,
    exposedHeaders: ["Set-Cookie"],
  })
);
app.use(cookieParser());
app.use(express.json());

// Routes
app.use("/auth", authRoute);
app.use("/user", userRoute);

// Cron jobs
if (process.env.NODE_ENV === "production") {
  cron.schedule("0 0 * * *", async () => {
    await sendEmail();
  });
}
